from google.appengine.ext import ndb

from exceptions import UnableToProcessException
from models import Task, Issue, Release


def set_task_status_and_propagate(release, issue, task, new_status):
    if new_status > Task.Status.NEW and release is None:
        raise UnableToProcessException('Cant update task status, issue #%s is not in any release' % issue.id)

    if new_status == Task.Status.NEW and Issue.Status.TO_DO_OR_FIX <= issue.status <= Issue.Status.DONE_OR_FIXED:
        set_task_status_and_propagate(release, issue, task, Task.Status.TO_DO)
        if issue.status == Issue.Status.DONE_OR_FIXED:
            issue.status = Issue.Status.IN_PROGRESS
        return

    if new_status == Task.Status.IN_PROGRESS:
        issue.status = Issue.Status.IN_PROGRESS
        task.status = new_status
        release.status = Release.Status.IN_PROGRESS
        return

    if new_status == Task.Status.DONE:
        task.status = new_status
        tasks = ndb.get_multi([tk for tk in issue.tasks if tk != task.key])
        all_done = True
        for t in tasks:
            all_done = all_done and t.status == Task.Status.DONE
        if all_done:
            issue.status = Issue.Status.DONE_OR_FIXED
            all_done = True
            issues = ndb.get_multi(release.issues)
            for i in issues:
                all_done = all_done and i.status == Issue.Status.DONE_OR_FIXED
            if all_done:
                release.status = Release.Status.COMPLETED
        return
    task.status = new_status
