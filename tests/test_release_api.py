import json
import unittest

from google.appengine.ext import ndb
from google.appengine.ext import testbed

from main import app
from models import Project, Release


class ReleasesApiTest(unittest.TestCase):
    def setUp(self):
        self.testbed = testbed.Testbed()
        self.testbed.activate()
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()

        # Clear ndb's in-context cache between tests.
        # This prevents data from leaking between tests.
        # Alternatively, you could disable caching by
        # using ndb.get_context().set_cache_policy(False)
        ndb.get_context().clear_cache()
        self.app = app.test_client()
        self.maxDiff = None

    def test_get_release(self):
        release = Release(version='1.0.0', status=Release.Status.IN_PROGRESS)
        release_key = release.put()

        expected_response = {
            u'version': u'1.0.0',
            u'status': Release.Status.IN_PROGRESS,
            u'key': release_key.urlsafe(),
            u'creation_date': release.creation_date.isoformat(),
            u'released_date': u''
        }

        response = self.app.get('/api/releases/%s' % release_key.urlsafe())

        self.assertEquals(response.status_code, 200)

        data = json.loads(response.get_data().decode('utf8'))
        self.assertEquals(data, expected_response)