import unittest

from api.status_update_rules import set_task_status_and_propagate
from api.exceptions import UnableToProcessException
from main import app
from google.appengine.ext import ndb
from google.appengine.ext import testbed

from models import Project, Issue, Task, Release


class StatusChangeRulesTest(unittest.TestCase):
    def setUp(self):
        self.testbed = testbed.Testbed()
        self.testbed.activate()
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()

        # Clear ndb's in-context cache between tests.
        # This prevents data from leaking between tests.
        # Alternatively, you could disable caching by
        # using ndb.get_context().set_cache_policy(False)
        ndb.get_context().clear_cache()
        self.app = app.test_client()
        self.maxDiff = None

    def test_cant_update_task_if_issue_on_backlog(self):
        project = Project(name='Test Project One', id='TPO')

        issue = Issue(title='Test Feature', kind='F', id='TPO.1', desc='This is a test feature',
                      status=Issue.Status.NEW)
        task = Task(id='TPO.1.1', title='Testing task', desc='', status=Task.Status.NEW)
        task.put()
        issue.tasks.append(task.key)
        issue.put()
        project.backlog.append(issue.key)
        project.put()

        exception_raised = False
        try:
            set_task_status_and_propagate(None, issue, task, Task.Status.TO_DO)
        except UnableToProcessException as e:
            exception_raised = True
            self.assertTrue('TPO.1' in e.message)
        self.assertTrue(exception_raised)

    def test_set_task_to_in_progress_moves_issue_and_release_to_in_progress(self):
        project = Project(name='Test Project One', id='TPO')
        issue = Issue(title='Test Feature', kind='F', id='TPO.1', desc='This is a test feature',
                      status=Issue.Status.NEW)
        task = Task(id='TPO.1.1', title='Testing task', desc='', status=Task.Status.NEW)
        task.put()
        issue.tasks.append(task.key)
        issue.put()
        project.backlog.append(issue.key)
        project.put()
        release = Release(version='0.1.0', project=project.key, status=Release.Status.NEW)
        release.issues.append(issue.key)
        release.put()
        issue.release = release.key
        issue.put()

        exception_raised = False
        try:
            set_task_status_and_propagate(release, issue, task, Task.Status.IN_PROGRESS)
        except UnableToProcessException as e:
            exception_raised = True

        self.assertFalse(exception_raised)
        self.assertEquals(Task.Status.IN_PROGRESS, task.status)
        self.assertEquals(Issue.Status.IN_PROGRESS, issue.status)
        self.assertEquals(Release.Status.IN_PROGRESS, release.status)

    def test_set_task_to_done_moves_issue_to_done_if_all_done(self):
        project = Project(name='Test Project One', id='TPO')
        issue = Issue(title='Test Feature', kind='F', id='TPO.1', desc='This is a test feature',
                      status=Issue.Status.NEW)
        issue2 = Issue(title='Test Feature 2', kind='F', id='TPO.2', desc='This is a test feature 2',
                       status=Issue.Status.IN_PROGRESS)
        task = Task(id='TPO.1.1', title='Testing task', desc='', status=Task.Status.NEW)
        task.put()
        task2 = Task(id='TPO.1.2', title='Testing task 2', desc='', status=Task.Status.DONE)
        task2.put()
        issue.tasks.append(task.key)
        issue.tasks.append(task2.key)
        issue.put()
        issue2.put()
        project.put()
        release = Release(version='0.1.0', project=project.key, status=Release.Status.IN_PROGRESS)
        release.issues.append(issue.key)
        release.issues.append(issue2.key)
        release.put()
        issue.release = release.key
        issue.put()

        exception_raised = False
        try:
            set_task_status_and_propagate(release, issue, task, Task.Status.DONE)
        except UnableToProcessException as e:
            exception_raised = True

        self.assertFalse(exception_raised)
        self.assertEquals(Task.Status.DONE, task.status)
        self.assertEquals(Issue.Status.DONE_OR_FIXED, issue.status)
        self.assertEquals(Release.Status.IN_PROGRESS, release.status)

    def test_set_task_to_done_do_not_move_issue_to_done_if_any_not_done(self):
        project = Project(name='Test Project One', id='TPO')
        issue = Issue(title='Test Feature', kind='F', id='TPO.1', desc='This is a test feature',
                      status=Issue.Status.IN_PROGRESS)
        task = Task(id='TPO.1.1', title='Testing task', desc='', status=Task.Status.NEW)
        task.put()
        task2 = Task(id='TPO.1.2', title='Testing task 2', desc='', status=Task.Status.IN_PROGRESS)
        task2.put()
        issue.tasks.append(task.key)
        issue.tasks.append(task2.key)
        issue.put()
        project.backlog.append(issue.key)
        project.put()
        release = Release(version='0.1.0', project=project.key, status=Release.Status.IN_PROGRESS)
        release.issues.append(issue.key)
        release.put()
        issue.release = release.key
        issue.put()

        exception_raised = False
        try:
            set_task_status_and_propagate(release, issue, task, Task.Status.DONE)
        except UnableToProcessException as e:
            exception_raised = True

        self.assertFalse(exception_raised)
        self.assertEquals(Task.Status.DONE, task.status)
        self.assertEquals(Issue.Status.IN_PROGRESS, issue.status)
        self.assertEquals(Release.Status.IN_PROGRESS, release.status)

    def test_set_task_to_done_moves_issue_to_done_and_release_to_complete(self):
        project = Project(name='Test Project One', id='TPO')
        issue = Issue(title='Test Feature', kind='F', id='TPO.1', desc='This is a test feature',
                      status=Issue.Status.NEW)
        task = Task(id='TPO.1.1', title='Testing task', desc='', status=Task.Status.NEW)
        task.put()
        task2 = Task(id='TPO.1.2', title='Testing task 2', desc='', status=Task.Status.DONE)
        task2.put()
        issue.tasks.append(task.key)
        issue.tasks.append(task2.key)
        issue.put()
        project.backlog.append(issue.key)
        project.put()
        release = Release(version='0.1.0', project=project.key, status=Release.Status.IN_PROGRESS)
        release.issues.append(issue.key)
        release.put()
        issue.release = release.key
        issue.put()

        exception_raised = False
        try:
            set_task_status_and_propagate(release, issue, task, Task.Status.DONE)
        except UnableToProcessException as e:
            exception_raised = True

        self.assertFalse(exception_raised)
        self.assertEquals(Task.Status.DONE, task.status)
        self.assertEquals(Issue.Status.DONE_OR_FIXED, issue.status)
        self.assertEquals(Release.Status.COMPLETED, release.status)

    def test_set_task_to_new_and_issue_in_todo_will_make_task_todo(self):
        project = Project(name='Test Project One', id='TPO')
        issue = Issue(title='Test Feature', kind='F', id='TPO.1', desc='This is a test feature',
                      status=Issue.Status.TO_DO_OR_FIX)
        task = Task(id='TPO.1.1', title='Testing task', desc='', status=Task.Status.NEW)
        task.put()
        issue.tasks.append(task.key)
        issue.put()
        project.backlog.append(issue.key)
        project.put()
        release = Release(version='0.1.0', project=project.key, status=Release.Status.IN_PROGRESS)
        release.issues.append(issue.key)
        release.put()
        issue.release = release.key
        issue.put()

        exception_raised = False
        try:
            set_task_status_and_propagate(release, issue, task, Task.Status.NEW)
        except UnableToProcessException as e:
            exception_raised = True

        self.assertFalse(exception_raised)
        self.assertEquals(Task.Status.TO_DO, task.status)
        self.assertEquals(Issue.Status.TO_DO_OR_FIX, issue.status)
        self.assertEquals(Release.Status.IN_PROGRESS, release.status)

    def test_set_task_to_new_and_issue_in_new_will_keep_new(self):
        project = Project(name='Test Project One', id='TPO')
        issue = Issue(title='Test Feature', kind='F', id='TPO.1', desc='This is a test feature',
                      status=Issue.Status.NEW)
        task = Task(id='TPO.1.1', title='Testing task', desc='', status=Task.Status.NEW)
        task.put()
        issue.tasks.append(task.key)
        issue.put()
        project.backlog.append(issue.key)
        project.put()
        release = Release(version='0.1.0', project=project.key, status=Release.Status.NEW)
        release.issues.append(issue.key)
        release.put()
        issue.release = release.key
        issue.put()

        exception_raised = False
        try:
            set_task_status_and_propagate(release, issue, task, Task.Status.NEW)
        except UnableToProcessException as e:
            exception_raised = True

        self.assertFalse(exception_raised)
        self.assertEquals(Task.Status.NEW, task.status)
        self.assertEquals(Issue.Status.NEW, issue.status)
        self.assertEquals(Release.Status.NEW, release.status)

    def test_set_task_to_new_and_issue_in_done_will_reopen_issue(self):
        project = Project(name='Test Project One', id='TPO')
        issue = Issue(title='Test Feature', kind='F', id='TPO.1', desc='This is a test feature',
                      status=Issue.Status.DONE_OR_FIXED)
        task = Task(id='TPO.1.1', title='Testing task', desc='', status=Task.Status.NEW)
        task.put()
        issue.tasks.append(task.key)
        issue.put()
        project.backlog.append(issue.key)
        project.put()
        release = Release(version='0.1.0', project=project.key, status=Release.Status.IN_PROGRESS)
        release.issues.append(issue.key)
        release.put()
        issue.release = release.key
        issue.put()

        exception_raised = False
        try:
            set_task_status_and_propagate(release, issue, task, Task.Status.NEW)
        except UnableToProcessException as e:
            exception_raised = True

        self.assertFalse(exception_raised)
        self.assertEquals(Task.Status.TO_DO, task.status)
        self.assertEquals(Issue.Status.IN_PROGRESS, issue.status)
        self.assertEquals(Release.Status.IN_PROGRESS, release.status)

