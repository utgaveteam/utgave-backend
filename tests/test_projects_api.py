import json
import unittest

from google.appengine.datastore import datastore_stub_util
from google.appengine.ext import ndb
from google.appengine.ext import testbed

from main import app
from models import Project, Release, Issue


class ProjectsApiTest(unittest.TestCase):
    def setUp(self):
        self.testbed = testbed.Testbed()
        self.testbed.activate()
        # Create a consistency policy that will simulate the High Replication consistency model.
        self.policy = datastore_stub_util.PseudoRandomHRConsistencyPolicy(probability=1)
        self.testbed.init_datastore_v3_stub(consistency_policy=self.policy)
        self.testbed.init_memcache_stub()

        # Clear ndb's in-context cache between tests.
        # This prevents data from leaking between tests.
        # Alternatively, you could disable caching by
        # using ndb.get_context().set_cache_policy(False)
        ndb.get_context().clear_cache()
        self.app = app.test_client()
        self.maxDiff = None

    def test_get_all_projects(self):
        project1 = Project(name='Test Project One', id='TPO')
        project1.put()
        project2 = Project(name='Test Project Two', id='TPT')
        project2.put()

        expected_response = [
            {
                u'id': u'TPO',
                u'name': u'Test Project One',
                u'key': project1.key.urlsafe(),
                u'creation_date': project1.creation_date.isoformat()
            },
            {
                u'id': u'TPT',
                u'name': u'Test Project Two',
                u'key': project2.key.urlsafe(),
                u'creation_date': project2.creation_date.isoformat(),
            }
        ]

        response = self.app.get('/api/projects')

        self.assertEquals(response.status_code, 200)

        data = json.loads(response.get_data().decode('utf8'))
        self.assertEquals(data, expected_response)

    def test_get_project(self):
        project = Project(name='Test Project One', id='TPO')
        p_key = project.put()

        expected_response = {
            u'id': u'TPO',
            u'name': u'Test Project One',
            u'key': p_key.urlsafe(),
            u'creation_date': project.creation_date.isoformat(),
        }

        response = self.app.get('/api/projects/%s' % p_key.urlsafe())

        self.assertEquals(response.status_code, 200)

        data = json.loads(response.get_data().decode('utf8'))
        self.assertEquals(data, expected_response)

    def test_save_new_project(self):
        body = {
            u'id': u'TPO',
            u'name': u'Test Project One',
        }

        response = self.app.post('/api/projects', data=json.dumps(body), content_type='application/json')

        projects = Project.query().fetch(1)
        self.assertEquals(len(projects), 1)
        project = projects[0]

        expected_response = {
            u'id': u'TPO',
            u'name': u'Test Project One',
            u'key': project.key.urlsafe(),
            u'creation_date': project.creation_date.isoformat(),
        }

        self.assertEquals(response.status_code, 200)

        data = json.loads(response.get_data().decode('utf8'))
        self.assertEquals(data, expected_response)

    def test_get_backlog(self):
        issue1 = Issue(id='TPO.1', title='Test Feature', desc='This is a test feature', status=Issue.Status.NEW,
                       kind='F')
        issue2 = Issue(id='TPO.2', title='Test Bug', desc='This is a test Bug', status=Issue.Status.NEW, kind='B')
        issue1.put()
        issue2.put()

        project = Project(name='Test Project One', id='TPO')
        project.backlog.append(issue1.key)
        project.backlog.append(issue2.key)
        project_key = project.put()

        expected_response = [
            {
                u'id': u'TPO.1',
                u'title': u'Test Feature',
                u'desc': u'This is a test feature',
                u'status': Issue.Status.NEW,
                u'key': issue1.key.urlsafe(),
                u'creation_date': issue1.creation_date.isoformat(),
                u'kind': u'F'
            },
            {
                u'id': u'TPO.2',
                u'title': u'Test Bug',
                u'desc': u'This is a test Bug',
                u'status': Issue.Status.NEW,
                u'key': issue2.key.urlsafe(),
                u'creation_date': issue2.creation_date.isoformat(),
                u'kind': u'B'
            }
        ]

        url = '/api/projects/%s/backlog' % project_key.urlsafe()

        response = self.app.get(url, content_type='application/json')

        self.assertEquals(response.status_code, 200)

        data = json.loads(response.get_data().decode('utf8'))
        self.assertEquals(data, expected_response)

    def test_get_releases(self):
        release1 = Release(version='1.0.0', status=Release.Status.NEW)
        r1_key = release1.put()
        release2 = Release(version='1.0.1', status=Release.Status.NEW)
        r2_key = release2.put()

        project = Project(name='Test Project One', id='TPO')
        project.releases.append(r1_key)
        project.releases.append(r2_key)
        project_key = project.put()

        expected_response = [
            {
                u'version': u'1.0.1',
                u'status': Release.Status.NEW,
                u'key': release2.key.urlsafe(),
                u'creation_date': release2.creation_date.isoformat(),
                u'released_date': u''
            },
            {
                u'version': u'1.0.0',
                u'status': Release.Status.NEW,
                u'key': release1.key.urlsafe(),
                u'creation_date': release1.creation_date.isoformat(),
                u'released_date': u''
            },
        ]

        url = '/api/projects/%s/releases' % project_key.urlsafe()

        response = self.app.get(url, content_type='application/json')

        self.assertEquals(response.status_code, 200)

        data = json.loads(response.get_data().decode('utf8'))
        self.assertEquals(data, expected_response)

    def test_save_release(self):
        project = Project(name='Test Project One', id='TPO')
        project_key = project.put()

        body = {
            u'version': u'1.0.0',
        }

        url = '/api/projects/%s/releases' % project_key.urlsafe()

        response = self.app.post(url, data=json.dumps(body), content_type='application/json')

        self.assertEquals(response.status_code, 200)

        project = project_key.get()
        self.assertIsNotNone(project)
        self.assertEquals(1, len(project.releases))
        release = project.releases[0].get()
        self.assertIsNotNone(release)
        self.assertEquals('1.0.0', release.version)
        self.assertEquals(Release.Status.NEW, release.status)

        expected_response = {
            u'version': u'1.0.0',
            u'status': Release.Status.NEW,
            u'key': release.key.urlsafe(),
            u'creation_date': release.creation_date.isoformat(),
            u'released_date': u''
        }

        data = json.loads(response.get_data().decode('utf8'))
        self.assertEquals(data, expected_response)

    def test_save_issue(self):
        project = Project(name='Test Project One', id='TPO', issue_count=0)
        project_key = project.put()

        body = {
            u'title': u'Test Bug',
            u'kind': u'B',
            u'desc': u''
        }

        url = '/api/projects/%s/issues' % project_key.urlsafe()

        response = self.app.post(url, data=json.dumps(body), content_type='application/json')

        self.assertEquals(response.status_code, 200)

        project = project_key.get()
        self.assertIsNotNone(project)
        self.assertEquals(1, len(project.backlog))
        issue = project.backlog[0].get()
        self.assertIsNotNone(issue)
        self.assertEquals('Test Bug', issue.title)
        self.assertEquals('TPO.1', issue.id)
        self.assertEquals(Issue.Status.NEW, issue.status)

        expected_response = {
            u'id': u'TPO.1',
            u'title': u'Test Bug',
            u'desc': u'',
            u'status': Issue.Status.NEW,
            u'key': issue.key.urlsafe(),
            u'creation_date': issue.creation_date.isoformat(),
            u'kind': u'B'
        }

        data = json.loads(response.get_data().decode('utf8'))
        self.assertEquals(data, expected_response)
