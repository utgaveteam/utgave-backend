import json

from flask import Response
from flask import request
from google.appengine.ext import ndb

from api import api
from models import Project, Issue, Release
from schemas import project_schema, projects_schema, release_schema, releases_schema, \
    issue_schema, issues_schema

RETURN_CHILD_COUNT = 10


@api.route('/projects', methods=['GET'])
def get_projects():
    projects = Project.query().fetch()
    return json.dumps(projects_schema(projects))


@api.route('/projects', methods=['POST'])
def save_project():
    project_dict = request.json
    # This is optimistic
    project_key = Project(name=project_dict['name'], id=project_dict['id'], desc=project_dict['desc']).put()

    return get_project(project_key.urlsafe())


@api.route('/projects/<key>')
def get_project(key):
    project = ndb.Key(urlsafe=key).get()
    return json.dumps(project_schema(project))


@api.route('/projects/id/<project_id>')
def get_project_by_id(project_id):
    project = Project.query().filter(Project.id == project_id).get()
    if project is None:
        return Response(status=404)
    return json.dumps(project_schema(project))


@api.route('/projects/<project_key>/releases', methods=['GET'])
def get_project_releases(project_key):
    project = ndb.Key(urlsafe=project_key).get()
    releases = ndb.get_multi(project.releases)

    if releases is not None and len(releases) > 1:
        releases.sort(key=lambda r: r.creation_date, reverse=True)

    return json.dumps(releases_schema(releases))


@api.route('/projects/<project_key>/releases', methods=['POST'])
def save_new_release(project_key):
    project = ndb.Key(urlsafe=project_key).get()

    release_dict = request.json
    release = Release(version=release_dict['version'], status=Release.Status.NEW, project=project.key)
    release_key = release.put()

    project.releases.append(release_key)
    project.put()

    return json.dumps(release_schema(release))


@api.route('/projects/<project_key>/backlog', methods=['GET'])
def get_project_backlog(project_key):
    project = ndb.Key(urlsafe=project_key).get()

    backlog = ndb.get_multi(project.backlog)

    return json.dumps(issues_schema(backlog))


@api.route('/projects/<project_key>/issues', methods=['POST'])
@ndb.transactional(xg=True)
def save_new_issue(project_key):
    project = ndb.Key(urlsafe=project_key).get()
    project.issue_count += 1

    payload_dict = request.json
    status = Issue.Status.NEW

    issue = Issue(title=payload_dict['title'], status=status, kind=payload_dict['kind'], project=project.key)
    issue.desc = payload_dict['desc'] if 'desc' in payload_dict else ''
    issue.id = '%s.%d' % (project.id, project.issue_count)
    issue.put()

    project.backlog.append(issue.key)
    project.put()

    return json.dumps(issue_schema(issue))


@api.route('/admin/update/issues', methods=['GET'])
def update_issue_states():

    projects = Project.query().fetch()
    issues_to_delete = []
    for p in projects:
        for k in p.backlog:
            if k.get() is None:
                issues_to_delete.append(k)
        p.backlog = [k for k in p.backlog if k not in issues_to_delete]
        p.put()


    return 'OK'