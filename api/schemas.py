def project_schema(project):
    return {
        'id': project.id,
        'name': project.name,
        'desc': project.desc,
        'key': project.key.urlsafe(),
        'creation_date': project.creation_date.isoformat()
    }


def projects_schema(projects):
    return [project_schema(p) for p in projects]


def release_schema(release):
    return {
        'version': release.version,
        'status': release.status,
        'key': release.key.urlsafe(),
        'creation_date': release.creation_date.isoformat(),
        'released_date': release.released_date.isoformat() if release.released_date else ''
    }


def releases_schema(releases):
    return [release_schema(r) for r in releases]


def issue_schema(issue):
    return {
        'id': issue.id,
        'title': issue.title,
        'desc': issue.desc,
        'status': issue.status,
        'key': issue.key.urlsafe(),
        'creation_date': issue.creation_date.isoformat(),
        'kind': issue.kind
    }


def issues_schema(bugs):
    return [issue_schema(b) for b in bugs]


def task_schema(task):
    return {
        'id': task.id,
        'title': task.title,
        'desc': task.desc,
        'status': task.status,
        'key': task.key.urlsafe(),
        'creation_date': task.creation_date.isoformat()
    }


def tasks_schema(tasks):
    return [task_schema(t) for t in tasks]