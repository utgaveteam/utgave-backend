import json
import unittest

from google.appengine.ext import ndb
from google.appengine.ext import testbed

from main import app
from models import Project, Release, Issue, Task


class ProjectsApiTest(unittest.TestCase):
    def setUp(self):
        self.testbed = testbed.Testbed()
        self.testbed.activate()
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()

        # Clear ndb's in-context cache between tests.
        # This prevents data from leaking between tests.
        # Alternatively, you could disable caching by
        # using ndb.get_context().set_cache_policy(False)
        ndb.get_context().clear_cache()
        self.app = app.test_client()
        self.maxDiff = None

    def test_get_issue(self):
        issue = Issue(title='Test Feature', kind='F', id='TPO.1', desc='This is a test feature',
                      status=Issue.Status.NEW)
        issue.put()

        expected_response = {
            u'id': u'TPO.1',
            u'title': u'Test Feature',
            u'desc': u'This is a test feature',
            u'status': Issue.Status.NEW,
            u'key': issue.key.urlsafe(),
            u'creation_date': issue.creation_date.isoformat(),
            u'kind': u'F'
        }

        response = self.app.get('/api/issues/%s' % issue.key.urlsafe())

        self.assertEquals(response.status_code, 200)

        data = json.loads(response.get_data().decode('utf8'))
        self.assertEquals(data, expected_response)

    def test_get_issue_tasks(self):
        issue = Issue(title='Test Feature', kind='F', id='TPO.1', desc='This is a test feature',
                      status=Issue.Status.NEW)
        task = Task(id='TPO.2.3', title='Test task', desc='', status=Task.Status.NEW)
        task.put()
        issue.tasks.append(task.key)
        issue.put()

        expected_response = [{
            u'id': u'TPO.2.3',
            u'title': u'Test task',
            u'desc': u'',
            u'status': Task.Status.NEW,
            u'key': task.key.urlsafe(),
            u'creation_date': task.creation_date.isoformat(),
        }]

        response = self.app.get('/api/issues/%s/tasks' % issue.key.urlsafe())

        self.assertEquals(response.status_code, 200)

        data = json.loads(response.get_data().decode('utf8'))
        self.assertEquals(data, expected_response)

    def test_save_task(self):
        issue = Issue(title='Test Feature', kind='F', id='TPO.1', desc='This is a test feature',
                      status=Issue.Status.NEW)
        issue.put()

        body = {
            'title': 'Test task'
        }

        response = self.app.post('/api/issues/%s/tasks' % issue.key.urlsafe(), data=json.dumps(body),
                                 content_type='application/json')

        self.assertEquals(response.status_code, 200)

        issue = issue.key.get()
        self.assertEquals(len(issue.tasks), 1)

        task = issue.tasks[0].get()
        self.assertEquals(task.id, 'TPO.1.1')
        self.assertEquals(task.title, 'Test task'),
        self.assertEquals(task.desc, '')
        self.assertEquals(task.status, Task.Status.NEW)

        expected_response = {
            u'id': u'TPO.1.1',
            u'title': u'Test task',
            u'desc': u'',
            u'status': Task.Status.NEW,
            u'key': task.key.urlsafe(),
            u'creation_date': task.creation_date.isoformat(),
        }

        data = json.loads(response.get_data().decode('utf8'))
        self.assertEquals(data, expected_response)
