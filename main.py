from flask import Flask
from api import api as api_blueprint
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

# Register api routes
app.register_blueprint(api_blueprint, url_prefix='/api')


