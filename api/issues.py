import json

from flask import request
from google.appengine.ext import ndb

from api import api
from models import Task, Issue, Release
from schemas import issue_schema, tasks_schema, task_schema
from status_update_rules import set_task_status_and_propagate


@api.route('/issues/<issue_key>', methods=['GET'])
def get_issue(issue_key):
    issue = ndb.Key(urlsafe=issue_key).get()
    return json.dumps(issue_schema(issue))


@api.route('/issues/<issue_key>/tasks', methods=['GET'])
def get_issue_tasks(issue_key):
    issue = ndb.Key(urlsafe=issue_key).get()
    tasks = ndb.get_multi(issue.tasks)
    return json.dumps(tasks_schema(tasks))


@api.route('/issues/<issue_key>/tasks', methods=['POST'])
def save_issue_task(issue_key):
    issue = ndb.Key(urlsafe=issue_key).get()
    task_dict = request.json

    task = Task(title=task_dict['title'], desc='')
    task.id = '%s.%d' % (issue.id, len(issue.tasks) + 1)
    task.status = Task.Status.NEW
    task.put()
    issue.tasks.append(task.key)
    release = None if issue.release is None else issue.release.get()

    set_task_status_and_propagate(release, issue, task, task.Status.NEW)

    task.put()
    issue.put()
    if release is not None:
        release.put()
    return json.dumps(task_schema(task))


@api.route('/issues/<issue_key>/tasks/<task_key>', methods=['PUT'])
def update_issue_task(issue_key, task_key):
    issue = ndb.Key(urlsafe=issue_key).get()
    task = ndb.Key(urlsafe=task_key).get()
    release = None if issue.release is None else issue.release.get()
    task_dict = request.json

    task.title = task_dict['title']
    task.desc = task_dict['desc']
    new_status = int(task_dict['status'])
    set_task_status_and_propagate(release, issue, task, new_status)
    task.put()
    issue.put()
    if release is not None:
        release.put()

    return json.dumps(task_schema(task))


@api.route('/issues/<issue_key>', methods=['PUT'])
@ndb.transactional(xg=True)
def update_issue(issue_key):
    issue = ndb.Key(urlsafe=issue_key).get()
    payload_dict = request.json

    issue.title = payload_dict['title']
    issue.desc = payload_dict['desc']
    issue.kind = payload_dict['kind']
    issue.put()

    return json.dumps(issue_schema(issue))
