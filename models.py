from google.appengine.ext import ndb


class Project(ndb.Model):
    id = ndb.StringProperty(required=True)
    name = ndb.StringProperty(required=True)
    desc = ndb.StringProperty()
    releases = ndb.KeyProperty(repeated=True)
    backlog = ndb.KeyProperty(repeated=True)
    creation_date = ndb.DateTimeProperty(required=True, auto_now_add=True)
    issue_count = ndb.IntegerProperty(default=0)


class Release(ndb.Model):
    # noinspection PyClassHasNoInit
    class Status:
        NEW = 100
        TO_DO = 150
        IN_PROGRESS = 200
        COMPLETED = 250
        RELEASED = 300

    project = ndb.KeyProperty()
    version = ndb.StringProperty(required=True)
    status = ndb.IntegerProperty(required=True)
    issues = ndb.KeyProperty(repeated=True)
    creation_date = ndb.DateTimeProperty(required=True, auto_now_add=True)
    released_date = ndb.DateTimeProperty(required=False)


class Task(ndb.Model):

    # noinspection PyClassHasNoInit
    class Status:
        NEW = 100
        TO_DO = 200
        IN_PROGRESS = 300
        DONE = 400

    id = ndb.StringProperty(required=True)
    title = ndb.StringProperty(required=True)
    desc = ndb.TextProperty(required=True)
    status = ndb.IntegerProperty(required=True)
    creation_date = ndb.DateTimeProperty(required=True, auto_now_add=True)


class Issue(ndb.Model):

    # noinspection PyClassHasNoInit
    class BugStatus:
        NEW = 100
        TO_FIX = 200
        IN_PROGRESS = 300
        DONE = 400

    # noinspection PyClassHasNoInit
    class FeatureStatus:
        NEW = 150
        TO_DO = 250
        IN_PROGRESS = 350
        DONE = 450

    # noinspection PyClassHasNoInit
    class Status:
        NEW = 1000
        TO_DO_OR_FIX = 2000
        IN_PROGRESS = 3000
        DONE_OR_FIXED = 4000

    id = ndb.StringProperty(required=True)
    title = ndb.StringProperty(required=True)
    desc = ndb.TextProperty()
    status = ndb.IntegerProperty(required=True)
    version = ndb.StringProperty()
    kind = ndb.StringProperty(default='F')
    tasks = ndb.KeyProperty(kind=Task, repeated=True)
    creation_date = ndb.DateTimeProperty(required=True, auto_now_add=True)
    project = ndb.KeyProperty()
    release = ndb.KeyProperty()
