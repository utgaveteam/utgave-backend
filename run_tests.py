#!/usr/bin/python
import sys
import os
import warnings

import unittest2
import sys
import os


def fix_path(sdk_path):
    sys.path.insert(0, os.path.join(os.path.abspath('.'), 'lib'))
    sys.path.insert(0, os.path.join(sdk_path, 'lib', 'fancy_urllib'))

    try:
        from google.appengine.api import apiproxy_stub_map
    except ImportError, e:
        # Hack to fix reports of import errors on Ubuntu 9.10.
        if 'google' in sys.modules:
            del sys.modules['google']
        sys.path = [sdk_path] + sys.path

        # sys.path.extend([sdk_path + "/lib/yaml/lib",
        #                  sdk_path + "/lib/fancy_urllib",
        #                  sdk_path + "/lib/webob"])


# silences Python's complaints about imports
warnings.filterwarnings('ignore', category=UserWarning)

USAGE = """
Path to your sdk must be the first argument. To run type:

$ run_tests.py path/to/your/appengine/installation

Remember to set environment variable FLASK_CONF to TEST.
Loading configuration depending on the value of
environment variable allows you to add your own
testing configuration in src/application/settings.py

"""


def main(sdk_path, test_path):
    print ('Looking for tests in: ' + test_path)
    fix_path(sdk_path)
    suite = unittest2.loader.TestLoader().discover(test_path)
    unittest2.TextTestRunner(verbosity=2).run(suite)


if __name__ == '__main__':
    try:
        # Path to the SDK installation
        SDK_PATH = sys.argv[1]  # ...or hardcoded path
        # Path to tests folder
        TEST_PATH = os.path.join(os.path.dirname(os.path.abspath(__name__)), 'tests')
        main(SDK_PATH, TEST_PATH)
    except IndexError:
        # you probably forgot about path as first argument
        print USAGE
