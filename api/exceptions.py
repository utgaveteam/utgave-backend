class UnableToProcessException(Exception):
    def __init__(self, message, *args, **kwargs):
        super(UnableToProcessException, self).__init__(*args, **kwargs)
        self.status_code = 400
        self.message = message

    def to_dict(self):
        rv = {'message': self.message}
        return rv
