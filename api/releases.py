import json

import datetime
from flask import request
from google.appengine.ext import ndb

from api import api
from exceptions import UnableToProcessException
from models import Issue, Release, Task
from schemas import release_schema, issues_schema, issue_schema


@api.route('/releases/<release_key>', methods=['GET'])
def get_release(release_key):
    release = ndb.Key(urlsafe=release_key).get()
    return json.dumps(release_schema(release))


@api.route('/releases/<release_key>/issues', methods=['GET'])
def get_release_issues(release_key):
    release = ndb.Key(urlsafe=release_key).get()
    issues = ndb.get_multi(release.issues)
    return json.dumps(issues_schema(issues))


@api.route('/releases/<release_key>/issues/<issue_key>', methods=['PUT'])
@ndb.transactional(xg=True)
def move_issue(release_key, issue_key):
    release = ndb.Key(urlsafe=release_key).get()
    issue = ndb.Key(urlsafe=issue_key).get()
    project = release.project.get()

    project.backlog.remove(issue.key)
    project.put()
    issue.release = release.key
    issue.status = Issue.Status.TO_DO_OR_FIX
    issue.put()
    if issue.tasks and len(issue.tasks) > 0:
        tasks = ndb.get_multi(issue.tasks)
        for t in tasks:
            t.status = Task.Status.TO_DO
            t.put()

    release.issues.append(issue.key)
    release.status = Release.Status.TO_DO if release.status < Release.Status.TO_DO else release.status
    release.put()

    return json.dumps(issue_schema(issue))


@api.route('/releases/<release_key>/issues/<issue_key>', methods=['DELETE'])
@ndb.transactional(xg=True)
def move_issue_to_backlog(release_key, issue_key):
    release = ndb.Key(urlsafe=release_key).get()
    issue = ndb.Key(urlsafe=issue_key).get()
    project = release.project.get()

    release.issues.remove(issue.key)
    release.put()
    project.backlog.append(issue.key)
    project.put()

    issue.release = None
    issue.status = Issue.Status.NEW
    issue.put()

    if issue.tasks and len(issue.tasks) > 0:
        tasks = ndb.get_multi(issue.tasks)
        for t in tasks:
            t.status = Task.Status.NEW
            t.put()

    return json.dumps(issue_schema(issue))


@api.route('/releases/<release_key>', methods=['PUT'])
def update_release(release_key):
    release = ndb.Key(urlsafe=release_key).get()
    release_dict = request.json
    release.version = release_dict['version']
    new_status = int(release_dict['status'])

    if new_status == Release.Status.RELEASED:
        if release.status != Release.Status.COMPLETED:
            raise UnableToProcessException('Release v%s is not yet completed' % release.version)
        release.released_date = datetime.datetime.now()

    release.status = new_status
    release.put()

    return json.dumps(release_schema(release))


@api.route('/releases/<release_key>/board', methods=['GET'])
def get_release_board(release_key):
    release = ndb.Key(urlsafe=release_key).get()

    response = {
        'release': {
            'version': release.version,
            'status': release.status,
            'key': release.key.urlsafe(),
            'issues': []
        },
    }

    issues = ndb.get_multi(release.issues)
    for i in issues:
        issue_tasks = ndb.get_multi(i.tasks)
        response_issue = {
            'title': i.title,
            'id': i.id,
            'key': i.key.urlsafe(),
            'kind': i.kind,
            'status': i.status,
            'tasks': {
                'TO_DO': [{
                              'title': t.title,
                              'id': t.id,
                              'key': t.key.urlsafe()
                          } for t in issue_tasks if t.status == Task.Status.TO_DO],
                'IN_PROGRESS': [{
                                    'title': t.title,
                                    'id': t.id,
                                    'key': t.key.urlsafe()
                                } for t in issue_tasks if t.status == Task.Status.IN_PROGRESS],
                'DONE': [{
                             'title': t.title,
                             'id': t.id,
                             'key': t.key.urlsafe()
                         } for t in issue_tasks if t.status == Task.Status.DONE],
            }
        }
        response['release']['issues'].append(response_issue)

    return json.dumps(response)
