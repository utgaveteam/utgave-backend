from flask import Blueprint, jsonify

api = Blueprint('api', __name__)

from . import projects, releases, issues, exceptions

__all__ = [projects, releases, issues, exceptions]


# PROJECT
# /projects                         GET     Get all projects with basic info
# /projects                         POST    Save a new project
# /projects/:project_key            GET     Get project
# /projects/:project_key/releases   GET     Get all releases for project with basic info
# /projects/:project_key/releases   POST    Save a new release for project
# /projects/:project_key/backlog    GET     Get project backlog
# /projects/:project_key/issues     POST    Save a new feature or bug

# ISSUES
# /issues/:issue_key                GET     Get issue
# /issues/:issue_key/tasks          GET     Get all tasks in issue
# /issues/:issue_key/tasks          POST    Save a new task on issue

# RELEASES
# /releases/:release_key                    GET     Get release
# /releases/:release_key/issues             GET     Get all issues on release
# /releases/:release_key/issues/:issue_key  PUT     Move issue to release

@api.errorhandler(exceptions.UnableToProcessException)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response
